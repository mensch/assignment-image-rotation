#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0

#include "../include/bmp_manager.h"

static uint32_t get_bytes_padding(const uint32_t width) {
    if (width % 4 == 0) {
        return 0;
    }
    return 4 - ((width * sizeof(struct pixel)) % 4);
}

static enum READ_STATUS read_header(FILE *file, struct bmp_header *bmp_header) {
    if (fread(bmp_header, sizeof(struct bmp_header), 1, file) == 1) {
        return READ_SUCCESS;
    }else{
        return READ_INVALID_HEADER;
    }
}

static size_t get_image_size(const struct image *image) {
    return (image->width * sizeof(struct pixel) + get_bytes_padding(image->width)) * image->height;
}

static size_t get_file_size(const struct image *image) {
    return get_image_size(image) + sizeof(struct bmp_header);
}

static struct bmp_header generate_bmp_header(const struct image *image) {
    return (struct bmp_header) {
            .bfType = BF_TYPE,
            .bfileSize = get_file_size(image),
            .bfReserved = BF_RESERVED,
            .bOffBits = 54,
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = get_image_size(image),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum READ_STATUS from_bmp(FILE *in, struct image *image) {

    struct bmp_header bmp_header = {0};
    if (read_header(in, &bmp_header) != READ_SUCCESS) {
        return READ_INVALID_HEADER;
    }
    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);

    const size_t padding = get_bytes_padding(image->width);

    for (size_t i = 0; i < image->height; i++) {
        if (fread((image->width*i + image->data), sizeof(struct pixel),
                  bmp_header.biWidth, in) != bmp_header.biWidth) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_INVALID_SIGNATURE;
        }
    }
    return READ_SUCCESS;
}

enum WRITE_STATUS to_bmp(FILE *out, const struct image *image) {
    struct bmp_header bmp_header = generate_bmp_header(image);

    if (!fwrite(&bmp_header, sizeof(struct bmp_header), 1, out)
            || (fseek(out, bmp_header.bOffBits, SEEK_SET) != 0)) {
        return WRITE_ERROR;
    }

    const uint8_t zeros[3] = { 0 };

    uint32_t padding = get_bytes_padding(image->width);

    if (image->data != NULL) {
        for (size_t i = 0; i < image->height; ++i) {
            if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) < 1) {
                return WRITE_ERROR;
            }
            if (fwrite(zeros, sizeof(uint8_t), padding, out) < padding) {
                return WRITE_ERROR;
            }
        }
    } else {
        return WRITE_ERROR;
    }

    return WRITE_SUCCESS;
}
