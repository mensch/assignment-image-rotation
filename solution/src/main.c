#include "../include/file_manager.h"
#include "../include/bmp_manager.h"
#include "../include/rotator.h"

const char* read_errors[] = {
        [READ_INVALID_SIGNATURE]="Source image has invalid signature!\n",
        [READ_INVALID_BITS]="Source image has invalid bits!\n",
        [READ_INVALID_HEADER]="Source image has invalid header!\n"
};

int main(int argc, char **argv) {
    if (argc < 3) {
        fprintf(stderr, "Required arguments: <source-image-path> <transformed-image-path>\n");
        return 1;
    }

    FILE *input_file;

    if (open_file(&input_file, argv[1], "r") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open source image for read!\n");
        return 2;
    }

    struct image source_image = {0};

    enum READ_STATUS readStatus = from_bmp(input_file, &source_image);
    if (readStatus != READ_SUCCESS) {
        fprintf(stderr, "%s", read_errors[readStatus]);
        return 3;
    }

    if (close_file(input_file) != CLOSE_SUCCESS) {
        fprintf(stderr, "Unable to close source image!\n");
    }

    struct image rotated_image = rotate(source_image);

    if (!rotated_image.data){
        fprintf(stderr, "Could not allocate memory!\n");
        return 100;
    }

    FILE *output_file;

    if (open_file(&output_file, argv[2], "w") != OPEN_SUCCESS) {
        fprintf(stderr, "Unable to open target image for write!\n");
        return 4;
    }

    if (to_bmp(output_file, &rotated_image) != WRITE_SUCCESS) {
        fprintf(stderr, "Unable to write into target image!\n");
        return 5;
    }

    if (close_file(output_file) != CLOSE_SUCCESS) {
        fprintf(stderr, "Unable to close target image!\n");
    }

    fprintf(stdout, "Success!\n");

    destroy_image(&source_image);
    destroy_image(&rotated_image);

    return 0;
}
